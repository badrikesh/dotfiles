#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '


#HISTCONTROL=$HISTCONTROL:ignoredups
#HISTCONTROL=$HISTCONTROL:ignorespace

HISTCONTROL=$HISTCONTROL:ignoreboth

shopt -s autocd

export HISTSIZE=2000
export HISTFILESIZE=2000

#export PS1='\[\e[1;31m\]\h \[\e[0m\]\[\e[1m\]> \[\e[33m\]\u \[\e[0m\]\[\e[1m\]: \[\e[34m\]\w \[\e[0m\]\[\e[1m\]\$ \[\e[0m\]'
export PS1='\[\e[1m\]\[\e[34m\]\w \[\e[0m\]\[\e[1m\]\$ \[\e[0m\]'

alias nls='exa --group-directories-first --long --header --grid'

alias e='exit'
alias c='clear'
alias h='history'

alias .1='cd ..'
alias ..='cd ..'
alias .2='cd ../..'
alias .3='cd ../../..'

alias ls='ls --color=auto --group-directories-first'
alias l='ls -CF'
alias ll='ls -l'
alias la='ls -A'
alias lla='ls -Al'

alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

alias rip='sudo rm -rf'

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

alias py='python'

alias builtins='compgen -b'

# Use all cores
alias make='make -j $(($(nproc)+1))'
alias smake='sudo make -j $(($(nproc)+1))'


function gc()   {
    nohup google-chrome-stable $1 &
}

function ffox()  {
    nohup firefox $1 &
}

function tbw()  {
    echo "GB Written: $(echo "scale=3; $(sudo /usr/sbin/smartctl -A /dev/sda | grep "Total_LBAs_Written" | awk '{print $10}') * 512 / 1073741824" | bc | sed ':a;s/\B[0-9]\{3\}\>/,&/;ta')"
}

function pip_pkgs   {
    pip list | cut -d " " -f 1 | tail -n `expr \`pip list | wc -l\` - 2`
}

function pac_pkgs   {
    #pacman -Qe | cut -d " " -f 1
    pacman -Qqe
}

function errors()   {
    echo -e "systemctl --failed:"
    systemctl --failed
    echo -e "\njournalctl -p 3 -xb:"
    journalctl -p 3 -xb
}

function screencast()   {
    adb kill-server
    adb start-server
    adb devices
    scrcpy
}

function startup-analysis   {
    echo -e "systemd-analyze:"
    systemd-analyze
    echo -e "\nsystemd-analyze blame:"
    systemd-analyze blame
    echo -e "\nsystemd-analyze critical-chain:"
    systemd-analyze critical-chain
}

function font-list  {
    fc-list | awk '{print $1}' | sed 's/://g'
}

alias pacman='pacman --color=auto'
alias spacman='sudo pacman --color=auto'
alias sysup='spacman -Syyu'
alias ins='spacman -S'
alias unins='spacman -Rns'
alias clrpaccache='spacman -Scc'
alias rmorph='spacman -Rns $(pacman -Qtdq)'

alias yay='yay --color=auto'

alias clrusrcache='rm -rf ~/.cache/*'


alias ssn='sudo shutdown now'
alias sr='sudo reboot'

alias explr='thunar . &'

alias vi='nvim'
alias vim='nvim'
alias svim='sudo nvim'

alias nvidia='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia'

export PATH=/home/badrikesh/.local/bin:$PATH
#export PATH=/var/lib/snapd/snap/bin:$PATH
#yay -Yc remove unwanted dependencies
